# Instalação
* Rodar os comandos para baixar a imagem do nemo e criar o container
    - Instalação do docker: https://docs.docker.com/engine/install/

`sudo docker pull nvcr.io/nvidia/nemo:23.04`

* Verificar se os drivers da nvidia estão instalados e configurados para o docker:    
    - Instalação dos drivers: https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/latest/install-guide.html

* Rodar o comando na pasta raiz do projeto para subir o container e criar o volume para vincular os arquivos para o diretorio /workspace/nemo/AsrTest

`sudo docker run --device /dev/snd:/dev/snd --runtime=nvidia -it --rm -v $(pwd):/workspace/nemo/AsrTest --shm-size=16g -p 8888:8888 -p 6006:6006 --ulimit memlock=-1 --ulimit stack=67108864 nvcr.io/nvidia/nemo:23.04`

* Dentro do container rodar o comando para instalar as dependências:

`apt-get update`
`apt-get install portaudio19-dev python3-pyaudio gcc-x86-64-linux-gnu -y`
`pip install -r AsrTest/requirements.txt`

* Rodar o código:
`python AsrTest/asrtest.py`


# Código
- O código em foi extraído e mesclado de 2 exemplos disponibilizados pela NVidia:
    1. Online_ASR_Microphone_Demo - Realiza a identificação dos dispositivos de entrada, captura o audio e envia para um modelo pré treinado processar e retornar em texto
        * https://github.com/NVIDIA/NeMo/blob/main/tutorials/asr/Online_ASR_Microphone_Demo.ipynb

    2. Machine Translation Models - Realiza a tradução de um texto de um idioma para outro
        * https://docs.nvidia.com/deeplearning/nemo/user-guide/docs/en/stable/nlp/machine_translation/machine_translation.html

